using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private AudioSource Audio;
    public AudioClip footsteps;
    public CharacterController Controller;
    public float speed = 3f;
   
    public float Gravity = -19.01f;
    public float jumpheight = 1f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;
    CharacterController crouch;

    private void Start()
    {
        crouch = gameObject.GetComponent<CharacterController>();
        Audio = GetComponent<AudioSource>();
        Audio.clip = footsteps;
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpheight * -2f * Gravity);
        }

        Vector3 move = transform.right * -z + transform.forward * x;
        Controller.Move(move * speed * Time.deltaTime);

        velocity.y += Gravity * Time.deltaTime;

        Controller.Move(velocity * Time.deltaTime);
        if (Input.GetAxis("Horizontal")>0 || Input.GetAxis("Vertical")>0)
        {
            if (!Audio.isPlaying)
            {
                Audio.Play();
            }
        }
        else
        {
            Audio.Stop();
        }
       
    }
}