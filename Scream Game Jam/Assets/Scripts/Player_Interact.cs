﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Player_Interact : MonoBehaviour
{
   public  Camera cam;
    public float range;
   private Animator anim;
    private Player_inventory inventory;
    private Door_condition condition;
    public Image crosshair;
    public TextMeshProUGUI message_text;

    private void Start()
    {
        crosshair.enabled = false;
        message_text.text = "";
        inventory = this.gameObject.GetComponent<Player_inventory>();
    }

    void Update()
    {
       
        RaycastHit hit;
        if (Physics.Raycast(transform.position,cam.transform.TransformDirection(Vector3.forward), out hit,range))
        {
            if (hit.transform.gameObject.tag == "Animated")
            {
                crosshair.enabled = true;
                anim = hit.transform.gameObject.GetComponent<Animator>();
                condition = hit.transform.gameObject.GetComponent<Door_condition>();
                //Debug.Log("YO");
                if (Input.GetMouseButtonDown(0)&&condition.conditional==false)
                {
                    anim.SetBool("Open", true);
                }
                else if (Input.GetMouseButtonDown(0) && condition.conditional == true)
                {
                    if (inventory.objects.Count > 0)
                    {
                        for (var i = 0; i < inventory.objects.Count; i++)
                        {
                            if (inventory.objects[i] == condition.tag)
                            {
                                anim.SetBool("Open", true);
                            }
                            else
                            {
                                message_text.text = condition.message;
                                Invoke("RemoveWarning", 3f);
                            }
                        }

                    }
                    else
                    {
                        message_text.text = condition.message;
                        Invoke("RemoveWarning", 3f);
                    }
                }
            }

          

          

            else if (hit.transform.gameObject.tag == "Tool")
            {
                crosshair.enabled = true;
                if (Input.GetMouseButtonDown(0))
                {
                    inventory.objects.Insert(0, hit.transform.gameObject.tag);
                    Destroy(hit.transform.gameObject);
                    crosshair.enabled = false;
                }
            }
            else
            {
                crosshair.enabled = false;
            }
        }
      
    }
    void RemoveWarning()
    {
        message_text.text = "";
    }
}
