﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_randomwalk : MonoBehaviour
{
    public GameObject[] controllers;
    public float range = 50f;
    private int _index = 0;
    private NavMeshAgent agent;
    private bool attack;
    public Animator animator;
    private GameObject player;
    public float speed = 20f;
   
    // Start is called before the first frame update
    void Start()
    {
        agent = transform.GetComponent<NavMeshAgent>();
        controllers = GameObject.FindGameObjectsWithTag("Controller");
        attack = false;
        player = GameObject.FindWithTag("Player");
        Change();
        
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (attack == false)
        {
            Rand();
        }
        else
        {
            _attack();
        }

        if (dist <= range)
        {
            attack = true;
            animator.SetBool("Walk", false);
            animator.SetBool("Transform", true);
            animator.SetBool("Chase", false);
            Invoke("Chasing", 0.5f);
        }
        else
        {
            attack = false;
            animator.SetBool("Walk", true);
            animator.SetBool("Transform", false);
            animator.SetBool("Chase", false);
        }
    }

    void Rand()
    {
        _index = (int)_index;
        //Debug.Log(_index);

        agent.SetDestination(controllers[_index].transform.position);
        agent.speed = speed;

        if (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance)
        {
           
            Change();
           
        }
    }
    private void Change()
    {

        _index++;
        if (_index > controllers.Length-1)
        {
            _index = 0;
         
        }
       
      
    }

    void _attack()
    {
        agent.SetDestination(player.transform.position);
        agent.speed = speed + 10;
      
    }

    void Chasing()
    {
         animator.SetBool("Walk", false);
        animator.SetBool("Transform", true);
        animator.SetBool("Chase", true);

    }

}
